var myApp = angular.module('myApp', [])
.factory('postsService', ['$http', function($http){


	return {
		getPosts: function() {
			return $http.get('https://jsonplaceholder.typicode.com/posts').then(function(response){
				return response.data;
			})
		}
	}

}])
.controller('postsCtrl', ['$http', '$scope', 'postsService', function($http, $scope, postsService){

	//sorting
	$scope.users = new Array();

	// order
	$scope.orderLabel = "Desc";
	$scope.order = "-";

	// orderby
	$scope.orderByLabel = 'Date';
	$scope.orderBy = 'id';

	// byUser
	$scope.byUserLabel = 'All Users';
	$scope.byUser = 'allposts';


	postsService.getPosts().then(function(response){
		angular.forEach(response, function(post, key){			
			if($scope.users.indexOf(post.userId) === -1) $scope.users.push(post.userId);
			var rating = Math.floor(Math.random() * 5) + 1;
			post.rating = rating;
		})
		$scope.posts = response;
	})

	$scope.filterByUser = function(){    	
    	return function(item){
    		if($scope.byUser === "allposts") return true;
    		else if(item.userId == $scope.byUser) return true;
		}
	}


	$scope.updateByUser = function(userId){
		$scope.byUser = userId;
		if(userId == "allposts") $scope.byUserLabel = 'All Users';
		else $scope.byUserLabel = 'User '+userId;
		$('.myselect ul').removeClass('active')
	}

	$scope.updateOrderBy = function(orderby, label){
		$scope.orderByLabel = label;
		$scope.orderBy = orderby;
		$('.myselect ul').removeClass('active')
	}

	$scope.updateOrder = function(order, label){
		$scope.orderLabel = label;
		$scope.order = order;
		$('.myselect ul').removeClass('active')
	}

	$('.myselect .trigger').on('click', function(event) {
		$('.myselect ul').removeClass('active')
		$(this).parent().find('ul').addClass('active')

	});

	$("body").on('click', function(event) {
		var target = $(event.target);
		if(!target.is('label') && !target.is('input') && !target.is('div.trigger')){
			$('.myselect ul').removeClass('active')
		}

		console.log(target);
	});

}])